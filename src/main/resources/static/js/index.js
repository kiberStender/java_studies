(function(){
  function ajax(url, method = "GET", contentType = "application/x-www-form-urlencoded", format = "json", params = {}){

    return new Promise(function(resolve, reject){
      let xhr = () => {
        if(window.XMLHttpRequest) return new XMLHttpRequest()
        else return new ActiveXObject("Microsoft.XMLHTTP")
      }

      let parseJson = (resp) => {
        if(resp) return JSON.parse(resp)
        else return resp
      }

      //convertObjectToQueryString = (mData) -> (mData.foldLeft "") (acc) -> ([key, value]) -> acc + "&#{key}=#{value}"

      let req = xhr()

      req.onreadystatechange = function(){
        if(this.readyState === 4){
          if(this.status === 200) {

            if(format === "json"){
              resolve(parseJson(this.response));
            } else {
              resolve(this.response);
            }
          } else {
            reject(new Error(this.statusText));
          }
        } else {
          console.log(this.readyState);
        }
      };

      req.onerror = function(){
        reject(new Error(this.statusText));
      }

      req.open(method, url, true);
      req.setRequestHeader("Content-Type", contentType);
      req.send();
    });
  }

  function loadFromRemoteServer() {
    ajax("/greetingJson")
      .then((json) => {
        var id_ = document.querySelector("#id");
        var text_ = document.querySelector("#text");

        id_.innerHTML = json.id;
        text_.innerHTML = json.content;
      })
      .catch((err) => {console.log(err)});
  }


  loadFromRemoteServer();
})();