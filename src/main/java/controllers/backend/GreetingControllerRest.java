package controllers.backend;

import model.Greeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import persistence.entity.AccUser;
import persistence.entity.AccountType;
import persistence.entity.Balance;
import persistence.repository.Repository;
import persistence.repository.AccUserRepository;
import persistence.repository.AccountTypeRepository;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by sirkleber on 25/05/16.
 */
@RestController
public class GreetingControllerRest {

    @Autowired
    @Qualifier(AccUserRepository.COMPONENT_NAME)
    private Repository<AccUser> accUserRepository;

    @Autowired
    @Qualifier(AccountTypeRepository.COMPONENT_NAME)
    private Repository<AccountType> accountTypeRepository;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(method = RequestMethod.GET, value = "/greetingJson")
    public Greeting greetingJson(@RequestParam(value="name", defaultValue="World") String name){

        accUserRepository.insert(new AccUser().withUserpass("12345").withUserMail("kleber.stender@gmail.com"));

        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/greetingJson/{name}")
    public Greeting greetingJsonUrl(@PathVariable(value = "name") String name){
        AccUser accUser = new AccUser()
                .withUserpass("12345")
                .withUserMail("felipe.marcelus@gmail.com");

        accUserRepository.insert(accUser);

        AccountType accountType = new AccountType()
                .withAccName("Wallet")
                .withClosingDay("10")
                .withDescription("gdjslaksçldksçl")
                .withAccUser(accUser);

        accountTypeRepository.insert(accountType);

        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
}