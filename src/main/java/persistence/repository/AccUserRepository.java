package persistence.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.entity.AccUser;
import persistence.entityManager.MyEntityManager;

import java.util.List;

@Component(AccUserRepository.COMPONENT_NAME)
public class AccUserRepository implements Repository<AccUser>{
    public final static String COMPONENT_NAME = "AccUserRepository";

    @Autowired
    private MyEntityManager entityManager;

    @Override
    public List<AccUser> selectAll() {
        return entityManager.execute(em -> em.createQuery("From AccUser").getResultList());
    }

    public void insert(AccUser u){
        entityManager.insertOrUpdate(em -> {
            em.persist(u);
            return null;
        });
    }

    @Override
    public void update(AccUser accUser) {
        entityManager.insertOrUpdate(em -> {
            em.merge(accUser);
            return null;
        });
    }

}
