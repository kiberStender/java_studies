package persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.entity.TransactionType;
import persistence.entityManager.MyEntityManager;

import java.util.List;

@Component(TransactionTypeRepository.COMPONENT_NAME)
public class TransactionTypeRepository implements Repository<TransactionType> {
    public static final String COMPONENT_NAME = "TransactionTypeRepository";

    @Autowired
    private MyEntityManager entityManager;

    @Override
    public List<TransactionType> selectAll() {
        return entityManager.execute(em -> em.createQuery("From TransactionType").getResultList());
    }

    @Override
    public void insert(TransactionType transactionType) {
        entityManager.insertOrUpdate(em -> {
            em.persist(transactionType);
            return null;
        });
    }

    @Override
    public void update(TransactionType transactionType) {
        entityManager.insertOrUpdate(em -> {
            em.merge(transactionType);
            return null;
        });
    }
}
