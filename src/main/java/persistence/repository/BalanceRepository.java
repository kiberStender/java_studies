package persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.entity.Balance;
import persistence.entityManager.MyEntityManager;

import java.util.List;

@Component(BalanceRepository.COMPONENT_NAME)
public class BalanceRepository implements Repository<Balance> {
    public final static String COMPONENT_NAME = "BalanceRepository";

    @Autowired
    private MyEntityManager entityManager;

    @Override
    public List<Balance> selectAll() {
        return entityManager.execute(em -> em.createQuery("From Balance").getResultList());
    }

    @Override
    public void insert(Balance balance) {
        entityManager.insertOrUpdate(em -> {
            em.persist(balance);
            return null;
        });
    }

    @Override
    public void update(Balance balance) {
        entityManager.insertOrUpdate(em -> {
            em.merge(balance);
            return null;
        });
    }
}
