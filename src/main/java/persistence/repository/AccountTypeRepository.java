package persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.entity.AccountType;
import persistence.entityManager.MyEntityManager;

import java.util.List;

@Component(AccountTypeRepository.COMPONENT_NAME)
public class AccountTypeRepository implements Repository<AccountType> {
    public static final String COMPONENT_NAME =  "AccountTypeRepository";

    @Autowired
    private MyEntityManager entityManager;

    @Override
    public List<AccountType> selectAll() {
        return entityManager.execute(em -> em.createQuery("From AccountType").getResultList());
    }

    @Override
    public void insert(AccountType accountType) {
        entityManager.insertOrUpdate(em -> {
            em.persist(accountType);
            return null;
        });
    }

    @Override
    public void update(AccountType accountType) {
        entityManager.insertOrUpdate(em -> {
            em.merge(accountType);
            return null;
        });
    }
}
