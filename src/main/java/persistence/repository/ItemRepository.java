package persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.entity.Item;
import persistence.entityManager.MyEntityManager;

import java.util.List;

@Component(ItemRepository.COMPONENT_NAME)
public class ItemRepository implements Repository<Item> {
    public static final String COMPONENT_NAME = "ItemRepository";

    @Autowired
    private MyEntityManager entityManager;

    @Override
    public List<Item> selectAll(){
        return entityManager.execute(em -> em.createQuery("From Item").getResultList());
    }

    @Override
    public void insert(Item item) {
        entityManager.insertOrUpdate(em -> {
            em.persist(item);
            return null;
        });
    }

    @Override
    public void update(Item item) {
        entityManager.insertOrUpdate(em -> {
            em.merge(item);
            return null;
        });
    }
}
