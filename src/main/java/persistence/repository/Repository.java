package persistence.repository;

import java.util.List;

public interface Repository<T> {

    List<T> selectAll();

    void insert(T t);

    void update(T t);
}
