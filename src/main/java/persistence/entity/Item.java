package persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "item")
public class Item implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column
    private String itemId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "transactiontypeid")
    private TransactionType transactionType;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "acctypeid")
    private AccountType acctype;

    @Column(name = "description")
    private String description;

    @Column(name = "item_value")
    private Double itemValue;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "purchase_date")
    private Date purchaseDate;

    public Item(String itemId, TransactionType transactionType, AccountType acctype, String description, Double itemValue, Date purchaseDate) {
        this.itemId = itemId;
        this.transactionType = transactionType;
        this.acctype = acctype;
        this.description = description;
        this.itemValue = itemValue;
        this.purchaseDate = purchaseDate;
    }

    public Item() {
        this.purchaseDate = new Date();
    }

    public String getItemid() {
        return itemId;
    }

    public void setItemid(String itemid) {
        this.itemId = itemid;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public AccountType getAccountType() {
        return acctype;
    }

    public void setAccountType(AccountType accountType) {
        this.acctype = accountType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getItemValue() {
        return itemValue;
    }

    public void setItemValue(Double itemValue) {
        this.itemValue = itemValue;
    }

    public Date getPurchasedate() {
        return purchaseDate;
    }

    public void setPurchasedate(Date purchasedate) {
        this.purchaseDate = purchasedate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return new EqualsBuilder()
                .append(itemId, item.itemId)
                .append(transactionType, item.transactionType)
                .append(acctype, item.acctype)
                .append(description, item.description)
                .append(itemValue, item.itemValue)
                .append(purchaseDate, item.purchaseDate)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(itemId)
                .append(transactionType)
                .append(acctype)
                .append(description)
                .append(itemValue)
                .append(purchaseDate)
                .build();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("itemId", itemId)
                .append("transactionType", transactionType)
                .append("description", description)
                .append("itemValue", itemValue)
                .append("purchaseDate", purchaseDate)
                .build();
    }
}
