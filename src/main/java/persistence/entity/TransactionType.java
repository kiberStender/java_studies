package persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "transactiontype")
public class TransactionType implements Serializable{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column
    private String transactionTypeId;

    @Column
    private String transactionTypeName;

    public TransactionType(String transactionTypeId, String transactionTypeName) {
        this.transactionTypeId = transactionTypeId;
        this.transactionTypeName = transactionTypeName;
    }

    public TransactionType() { }

    public String getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(String transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TransactionType that = (TransactionType) o;

        return new EqualsBuilder()
                .append(transactionTypeId, that.transactionTypeId)
                .append(transactionTypeName, that.transactionTypeName)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(transactionTypeId)
                .append(transactionTypeName)
                .build();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("transactionTypeId", transactionTypeId)
                .append("transactionTypeName", transactionTypeName)
                .build();
    }
}
