package persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Balance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column
    private String balanceId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "acctypeid")
    private AccountType accountType;

    @Column
    private Double calcBalance;

    @Column
    private Double realBalance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date balanceDate;

    public Balance(String balanceId, AccountType accountType, Double calcBalance, Double realBalance, Date balanceDate) {
        this.balanceId = balanceId;
        this.accountType = accountType;
        this.calcBalance = calcBalance;
        this.realBalance = realBalance;
        this.balanceDate = balanceDate;
    }

    public Balance() {
        this.balanceDate = new Date();
    }

    public String getBalanceid() {
        return balanceId;
    }

    public void setBalanceid(String balanceid) {
        this.balanceId = balanceid;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Double getCalculatedBalance() {
        return calcBalance;
    }

    public void setCalculatedBalance(Double calculatedBalance) {
        this.calcBalance = calculatedBalance;
    }

    public Double getRealBalance() {
        return realBalance;
    }

    public void setRealBalance(Double realBalance) {
        this.realBalance = realBalance;
    }

    public Date getBalanceDate() {
        return balanceDate;
    }

    public void setBalanceDate(Date balanceDate) {
        this.balanceDate = balanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Balance balance = (Balance) o;

        return new EqualsBuilder()
                .append(balanceId, balance.balanceId)
                .append(accountType, balance.accountType)
                .append(calcBalance, balance.calcBalance)
                .append(realBalance, balance.realBalance)
                .append(balanceDate, balance.balanceDate)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(balanceId)
                .append(accountType)
                .append(calcBalance)
                .append(realBalance)
                .append(balanceDate)
                .build();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("balanceId", balanceId)
                .append("calcBalance", calcBalance)
                .append("realBalance", realBalance)
                .append("balanceDate", balanceDate)
                .build();
    }
}
