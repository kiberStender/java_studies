package persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "accuser")
public class AccUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "accuserid")
    private String accUserId;
    @Column(name = "usermail")
    private String userMail;
    @Column(name = "userpass")
    private String userpass;

    @OneToMany(mappedBy = "accUser", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private List<AccountType> accounts;

    public AccUser(String accUserId, String userMail, String userpass, List<AccountType> accounts) {
        this.accUserId = accUserId;
        this.userMail = userMail;
        this.userpass = userpass;
        this.accounts = accounts;
    }

    public AccUser(String userMail, final String userpass) {
        this.userMail = userMail;
        this.userpass = userpass;
        accounts = new ArrayList<>();
    }

    public AccUser() {
        accounts = new ArrayList<>();
    }

    /*Getters and setters */

    public String getAccUserId() {
        return accUserId;
    }

    public void setAccUserId(String accUserId) {
        this.accUserId = accUserId;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public List<AccountType> getAccountTypes() {
        return accounts;
    }

    public void setAccountTypes(List<AccountType> accountTypes) {
        this.accounts = accountTypes;
    }

    /*Getters and setters */

    /*with pattern*/
    public AccUser withId(final String accUserId){
        return new AccUser(accUserId, userMail, userpass, accounts);
    }

    public AccUser withUserMail(final String userMail){
        return new AccUser(accUserId, userMail, userpass, accounts);
    }

    public AccUser withUserpass(final String userpass){
        return new AccUser(accUserId, userMail, userpass, accounts);
    }

    public AccUser withAccountTypes(final List<AccountType> accountTypes){
        return new AccUser(accUserId, userMail, userpass, accountTypes);
    }

    public AccUser withAccountType(final AccountType accountType){
        return new AccUser(accUserId, userMail, userpass, accounts.stream().map(x -> {
            if(x.getAccTypeId().equals(accountType.getAccTypeId())){
                return accountType;
            } else {
                return x;
            }
        }).collect(Collectors.toList()));
    }

    public AccUser addAccountType(final AccountType accountType){
        accounts.add(accountType);

        return new AccUser(accUserId, userMail, userpass, accounts);
    }

    /*with pattern*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AccUser accUser = (AccUser) o;

        return new EqualsBuilder()
                .append(accUserId, accUser.accUserId)
                .append(userMail, accUser.userMail)
                .append(userpass, accUser.userpass)
                .append(accounts, accUser.accounts)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(accUserId)
                .append(userMail)
                .append(userpass)
                .append(accounts)
                .build();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("accUserId", accUserId)
                .append("userMail", userMail)
                .append("userpass", userpass)
                .append("accounts", accounts)
                .build();
    }
}
