package persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "accounttype")
public class AccountType implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column
    private String accTypeId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "accuserid")
    private AccUser accUser;

    @Column
    private String description;

    @Column
    private String closingDay;

    @Column
    private String accName;

    @OneToMany(mappedBy = "acctype", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Item> items;

    @OneToMany(mappedBy = "accountType", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Balance> balances;

    public AccountType(String accTypeId, AccUser accUser, String description, String closingDay, String accName, Set<Item> items, Set<Balance> balances) {
        this.accTypeId = accTypeId;
        this.accUser = accUser;
        this.description = description;
        this.closingDay = closingDay;
        this.accName = accName;
        this.items = items;
        this.balances = balances;
    }

    public AccountType(String accTypeId, AccUser accUser, String description, String closingDay, String accName) {
        this.accTypeId = accTypeId;
        this.accUser = accUser;
        this.description = description;
        this.closingDay = closingDay;
        this.accName = accName;
        items = new HashSet<>();
        balances = new HashSet<>();
    }

    public AccountType() {
        items = new HashSet<>();
        balances = new HashSet<>();
    }

    /* Getters and setters */

    public String getAccTypeId() {
        return accTypeId;
    }

    public void setAccTypeId(String accTypeId) {
        this.accTypeId = accTypeId;
    }

    public AccUser getAccUser() {
        return accUser;
    }

    public void setAccUser(AccUser accUser) {
        this.accUser = accUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClosingDay() {
        return closingDay;
    }

    public void setClosingDay(String closingDay) {
        this.closingDay = closingDay;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Set<Balance> getBalances() {
        return balances;
    }

    public void setBalances(Set<Balance> balances) {
        this.balances = balances;
    }
    /* Getters and setters */

    /*with pattern */
    public AccountType withAccTypeId(final String accTypeId){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withAccUser(final AccUser accUser){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withDescription(final String description){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withClosingDay(final String closingDay){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withAccName(final String accName){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withItems(final Set<Item> items){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType addItem(final Item item){
        items.add(item);
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType withBalances(final Set<Balance> balances){
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }

    public AccountType addBalance(final Balance balance){
        balances.add(balance);
        return new AccountType(accTypeId, accUser, description, closingDay, accName, items, balances);
    }
    /*with pattern*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AccountType that = (AccountType) o;

        return new EqualsBuilder()
                .append(accTypeId, that.accTypeId)
                .append(accUser, that.accUser)
                .append(description, that.description)
                .append(closingDay, that.closingDay)
                .append(accName, that.accName)
                .append(items, that.items)
                .append(balances, that.balances)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(accTypeId)
                .append(accUser)
                .append(description)
                .append(closingDay)
                .append(accName)
                .append(items)
                .append(balances)
                .build();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("accTypeId", accTypeId)
                .append("description", description)
                .append("closingDay", closingDay)
                .append("accName", accName)
                .append("items", items)
                .append("balances", balances)
                .build();
    }
}
