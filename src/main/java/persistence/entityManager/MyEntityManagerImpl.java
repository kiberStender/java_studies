package persistence.entityManager;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.function.Function;

@Component()
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MyEntityManagerImpl implements MyEntityManager {
    private final static String PU = "org.hibernate.tutorial.jpa";
    private EntityManagerFactory emf;

    @PostConstruct
    public void init(){
        emf = Persistence.createEntityManagerFactory(PU);
    }

    @Override
    public <A> A execute(Function<EntityManager, A> f) {
        final EntityManager em = emf.createEntityManager();
        final EntityTransaction et = em.getTransaction();

        try {

            et.begin();

            A res = f.apply(em);

            et.commit();

            em.close();
            //emf.close();

            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void  insertOrUpdate(Function<EntityManager, Void> f){
        final EntityManager em = emf.createEntityManager();
        final EntityTransaction et = em.getTransaction();

        et.begin();

        f.apply(em);

        et.commit();

        em.close();
        //emf.close();
    }

    @Override
    public void closeEntityManager() {
        emf.close();
        emf = null;
    }
}
