package persistence.entityManager;

import javax.persistence.EntityManager;
import java.util.function.Function;


public interface MyEntityManager {
     <A> A execute(Function<EntityManager, A> f);

    void  insertOrUpdate(Function<EntityManager, Void> f);

    void closeEntityManager();
}
